# ReadME -> Reconstruction

This repository contains two notebooks (a) for single view 3D reconstruction of pedestrian trajectories and (b) Multi-view 3D reconstruction of pedestrian trajectories. Details about the working of the code are provided in `SingleViewRecons.pdf` and `sfm_hf.odp` respectively. 

To run the respective notebook, install the packages specified in the first cells of respective notebooks into either the virtual environment or conda environment.